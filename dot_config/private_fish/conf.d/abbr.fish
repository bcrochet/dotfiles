if status --is-interactive
    abbr cat "bat"
    abbr cd "z"
    abbr ll "exa -lag --header"
    abbr ls "exa"
    abbr ps "procs -t"
    abbr sshpo "ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no"
    abbr tree "exa --tree"
    abbr vi "nvim"
    abbr zz "z -"
end

if status --is-interactive
    /usr/local/bin/zoxide init fish | source
    abbr cd "z"
    abbr zz "z -"
end

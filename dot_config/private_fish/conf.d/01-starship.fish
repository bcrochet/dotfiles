if status --is-interactive
    # Commands to run in interactive sessions can go here
    /usr/local/bin/starship init fish | source
end


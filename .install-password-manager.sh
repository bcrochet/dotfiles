#!/bin/sh

# exit immediately if password-manager-binary is already in $PATH
type password-manager-binary >/dev/null 2>&1 && exit

case "$(uname -s)" in
Darwin)
    # commands to install password-manager-binary on Darwin
    brew install bitwarden-cli
    ;;
Linux)
    # commands to install password-manager-binary on Linux
    [ -e $HOME/.local/bin/bw ] && exit
    [ ! -d $HOME/.local/bin ] && mkdir -p $HOME/.local/bin
    BW_CLI_VERSION=2024.2.0
    curl -s -q -OL --output-dir /var/tmp https://github.com/bitwarden/clients/releases/download/cli-v${BW_CLI_VERSION}/bw-linux-${BW_CLI_VERSION}.zip
    pushd /var/tmp
    unzip -f bw-linux-${BW_CLI_VERSION}.zip
    chmod +x bw
    mv bw $HOME/.local/bin/bw
    rm bw-linux-${BW_CLI_VERSION}.zip
    popd
    # Do we need to login?
    if [ "$(bw status | jq '.status')" = "unauthenticated" ]; then
        bw login
    fi
    ;;
*)
    echo "unsupported OS"
    exit 1
    ;;
esac

